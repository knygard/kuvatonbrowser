import { useEffect, useState } from 'react';
import keyboardJS from 'keyboardjs';
import moment from 'moment';
import { BrowserRouter, Routes, Route, useNavigate, useParams } from 'react-router-dom';

import UIActionCreators from './actions/UIActionCreators';
import ImageActionCreators from './actions/ImageActionCreators';
import ActionOverlay from './components/ActionOverlay';
import UIStore from './stores/UIStore';
import ImageStore from './stores/ImageStore.js';

import { ActionButtonsPane } from './components/ActionButtonsPane';
import CommentsBar from './components/CommentsBar';
import ImageLoader from './components/ImageLoader';
import Rating from './components/Rating';
import ThumbnailPane from './components/ThumbnailPane';

import './styles/normalize.css';
import './styles/ionicons.css';
import './styles/variables.styl';
import './styles/animations.styl';
import './styles/main.styl';

moment.locale('fi');

let hideActionButtonsTimeout: undefined | ReturnType<typeof setTimeout>;
let mouseMoveThrottleTimeout: undefined | ReturnType<typeof setTimeout>;

function Main() {
  const navigate = useNavigate();
  const { imageId } = useParams();

  const [activeImage, setActiveImage] = useState(ImageStore.getActiveImage());
  const [uiState, setUi] = useState(UIStore.getUIState());

  useEffect(() => {
    const uiListener = () => setUi(UIStore.getUIState());
    const imageListener = () => setActiveImage(ImageStore.getActiveImage());

    UIStore.addChangeListener(uiListener);
    ImageStore.addChangeListener(imageListener);

    return () => {
      UIStore.removeChangeListener(uiListener);
      ImageStore.removeChangeListener(imageListener);
    };
  }, []);

  useEffect(() => {
    const imageIdParsed = imageId ? parseInt(imageId, 10) : undefined;

    if (imageIdParsed && isNaN(imageIdParsed)) {
      return;
    }

    ImageActionCreators.setActiveImage(imageIdParsed);
  }, []);

  useEffect(() => {
    if (activeImage) {
      navigate(`/${activeImage?.imageId}`, { replace: true });
    }
  }, [activeImage]);

  if (!activeImage) {
    return <div className="LoadingOverlay">Ladataan...</div>;
  }

  const showTitlePane = uiState.titlePane;
  const showThumbnailPane = uiState.thumbnailPane;
  const showComments = uiState.comments && activeImage.comments.length > 0;
  const showActionButtons = uiState.actionButtons;

  return (
    <div className="SlideShowPage">
      <div className="image-view">
        {showTitlePane && (
          <div className="title-bar">
            <div className="action-buttons"></div>

            <div className="title">
              <Rating rating={activeImage.rating} />
              {activeImage.title}
            </div>
          </div>
        )}

        <ActionButtonsPane visible={showActionButtons} image={activeImage} />

        <ImageLoader src={activeImage.imageUrl} />

        {showThumbnailPane && <ThumbnailPane activeImage={activeImage} />}
      </div>

      {showComments && <CommentsBar comments={activeImage.comments} />}
    </div>
  );
}

function App() {
  const [showHelp, setShowHelp] = useState(false);
  const [overlayMessage, setOverlayMessage] = useState<string | null | undefined>();
  // const [autoPlayIsOn, setAutoPlay] = useState(false);

  useEffect(() => {
    const uiListener = () => {
      setShowHelp(UIStore.getUIState().help);
      setOverlayMessage(UIStore.getOverlayMessage());
    };

    UIStore.addChangeListener(uiListener);

    return () => {
      UIStore.removeChangeListener(uiListener);
    };
  }, []);

  useEffect(() => {
    keyboardJS.bind('left', () => {
      ImageActionCreators.previousImage();
    });

    keyboardJS.bind('right', () => {
      ImageActionCreators.nextImage();
    });

    keyboardJS.bind('f', () => {
      UIActionCreators.toggleFullScreen();
    });

    keyboardJS.bind('c', () => {
      UIActionCreators.toggleComments();
    });

    keyboardJS.bind('t', () => {
      UIActionCreators.toggleTitlePane();
    });

    keyboardJS.bind('b', () => {
      UIActionCreators.toggleThumbnailPane();
    });

    keyboardJS.bind(
      'h',
      () => {
        UIActionCreators.showHelp();
      },
      () => {
        UIActionCreators.hideHelp();
      },
    );

    keyboardJS.bind('space', () => {
      if (ImageStore.autoPlayIsOn()) {
        ImageActionCreators.autoPlayOff();
        UIActionCreators.showUIMessage('Autoplay off');
      } else {
        ImageActionCreators.autoPlayOn();
        UIActionCreators.showUIMessage('Autoplay on');
      }
    });

    window.addEventListener('mousemove', () => {
      if (!mouseMoveThrottleTimeout) {
        UIActionCreators.showActionButtons();
        clearTimeout(hideActionButtonsTimeout);
        hideActionButtonsTimeout = setTimeout(() => {
          UIActionCreators.hideActionButtons();
        }, 500);

        mouseMoveThrottleTimeout = setTimeout(() => {
          mouseMoveThrottleTimeout = undefined;
        }, 100);
      }
    });
  }, []);

  return (
    <div style={{ height: '100%' }}>
      <BrowserRouter>
        <Routes>
          <Route path="/:imageId?" element={<Main />} />
        </Routes>
      </BrowserRouter>

      {!!overlayMessage && <ActionOverlay>{overlayMessage}</ActionOverlay>}

      {showHelp && (
        <ActionOverlay header="Help">
          <div className="info-container">
            <div className="info-content">
              <h3>Hotkeys</h3>
              <table>
                <tbody>
                  {[
                    { key: 'H', text: 'Show help' },
                    { key: 'B', text: 'Toggle thumbnails' },
                    { key: 'C', text: 'Toggle comments' },
                    { key: 'T', text: 'Toggle title bar' },
                    { key: 'F', text: 'Toggle fullscreen mode' },
                    { key: 'RIGHT arrow', text: 'Next picture' },
                    { key: 'LEFT arrow', text: 'Previous picture' },
                    // { key: 'SPACE', text: 'Toggle autoplay (8 sec. interval)' },
                  ].map((mapping) => (
                    <tr>
                      <td className="info-key">{mapping.key}</td>
                      <td className="info-text">{mapping.text}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </ActionOverlay>
      )}
    </div>
  );
}

export default App;
