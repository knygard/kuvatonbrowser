import AppDispatcher from '../dispatcher/AppDispatcher';

let autoPlayInterval: ReturnType<typeof setInterval> | undefined;

const autoPlayTime = 8000;

const ImageActions = {
  nextImage() {
    AppDispatcher.handleViewAction({
      actionType: 'NAVIGATE_NEXT_IMAGE',
    });
  },

  previousImage() {
    AppDispatcher.handleViewAction({
      actionType: 'NAVIGATE_PREVIOUS_IMAGE',
    });
  },

  init() {
    AppDispatcher.handleViewAction({
      actionType: 'INIT',
    });
  },

  setActiveImage(imageId?: number) {
    AppDispatcher.handleViewAction({
      actionType: 'SET_ACTIVE_IMAGE',
      imageId,
    });
  },

  autoPlayOn() {
    autoPlayInterval = setInterval(() => {
      this.nextImage();
    }, autoPlayTime);

    AppDispatcher.handleViewAction({
      actionType: 'AUTOPLAY_ON',
    });
  },
  autoPlayOff() {
    clearInterval(autoPlayInterval);

    AppDispatcher.handleViewAction({
      actionType: 'AUTOPLAY_OFF',
    });
  },
};

export default ImageActions;
