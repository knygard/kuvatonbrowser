import AppDispatcher from '../dispatcher/AppDispatcher';

const doc = document as any;

function toggleFullScreen() {
  if (
    !doc.fullscreenElement && // alternative standard method
    !doc.mozFullScreenElement &&
    !doc.webkitFullscreenElement &&
    !doc.msFullscreenElement
  ) {
    // current working methods
    if (doc.documentElement.requestFullscreen) {
      doc.documentElement.requestFullscreen();
    } else if (doc.documentElement.msRequestFullscreen) {
      doc.documentElement.msRequestFullscreen();
    } else if (doc.documentElement.mozRequestFullScreen) {
      doc.documentElement.mozRequestFullScreen();
    } else if (doc.documentElement.webkitRequestFullscreen) {
      doc.documentElement.webkitRequestFullscreen((Element as any).ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (doc.exitFullscreen) {
      doc.exitFullscreen();
    } else if (doc.msExitFullscreen) {
      doc.msExitFullscreen();
    } else if (doc.mozCancelFullScreen) {
      doc.mozCancelFullScreen();
    } else if (doc.webkitExitFullscreen) {
      doc.webkitExitFullscreen();
    }
  }
}

let UIMessageTimeout: undefined | ReturnType<typeof setTimeout>;
function resetUIMessage() {
  clearTimeout(UIMessageTimeout);
  UIMessageTimeout = setTimeout(() => {
    AppDispatcher.handleViewAction({
      actionType: 'OVERLAY_MESSAGE_RESET',
      UIMessage: '',
    });
  }, 500);
}

const UIActions = {
  toggleFullScreen() {
    toggleFullScreen();
    AppDispatcher.handleViewAction({
      actionType: 'FULL_SCREEN_TOGGLE',
    });
  },
  toggleComments() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_TOGGLE_COMMENTS',
      UIMessage: {
        visible: 'Comments visible',
        hidden: 'Comments hidden',
      },
    });

    resetUIMessage();
  },
  hideComments() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_HIDE_COMMENTS',
      UIMessage: 'Comments hidden',
    });
  },
  showComments() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_SHOW_COMMENTS',
      UIMessage: 'Comments visible',
    });
  },
  toggleTitlePane() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_TOGGLE_TITLE_PANE',
    });
  },
  toggleThumbnailPane() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_TOGGLE_THUMBNAIL_PANE',
    });
  },
  showActionButtons() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_SHOW_ACTIONBUTTONS',
    });
  },
  hideActionButtons() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_HIDE_ACTIONBUTTONS',
    });
  },
  showUIMessage(message: string) {
    AppDispatcher.handleViewAction({
      actionType: 'UI_SHOW_MESSAGE',
      UIMessage: message,
    });

    resetUIMessage();
  },

  showHelp() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_SHOW_HELP',
    });
  },
  hideHelp() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_HIDE_HELP',
    });
  },
  toggleHelp() {
    AppDispatcher.handleViewAction({
      actionType: 'UI_TOGGLE_HELP',
    });
  },
};

export default UIActions;
