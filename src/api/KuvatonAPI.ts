import axios from 'axios';

const KuvatonAPI = {
  images(limit: number, imageId?: number, direction?: 'previous' | 'next') {
    const params: Record<string, any> = {};

    if (imageId) {
      params.imageId = imageId;
    }

    params.limit = limit;
    params.direction = direction || 'next';

    return axios.get(import.meta.env.VITE_API_GET_IMAGES, { params });
  },
};

export default KuvatonAPI;
