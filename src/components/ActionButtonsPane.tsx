import classnames from 'classnames';

import UIActionCreators from '../actions/UIActionCreators';
import ImageActionCreators from '../actions/ImageActionCreators';

interface Props {
  visible: boolean;
  image: { browseUrl: string };
}

export const ActionButtonsPane = ({ visible, image }: Props) => {
  const autoPlayIsOn = false;

  return (
    <div className={classnames('ActionButtonsPane', { visible })}>
      <div className="ActionButtonsPane--container">
        <button className="action-button help" onClick={() => UIActionCreators.toggleHelp()}>
          <i className="ion-help" />
        </button>

        <a className="action-button ext-link" href={image.browseUrl} target="_blank">
          <i className="ion-android-share" />
        </a>

        <button
          className="action-button previous-image"
          onClick={() => {
            ImageActionCreators.previousImage();
          }}
        >
          <i className="ion-arrow-left-c" />
        </button>

        <button
          className="action-button next-image"
          onClick={() => ImageActionCreators.nextImage()}
        >
          <i className="ion-arrow-right-c" />
        </button>

        {!autoPlayIsOn && (
          <button
            className="action-button autoplay-on"
            title="Autoplay on"
            onClick={() => {
              UIActionCreators.showUIMessage('Autoplay on');
              ImageActionCreators.autoPlayOn();
            }}
          >
            <i className="ion-play" />
          </button>
        )}

        {autoPlayIsOn && (
          <button
            className="action-button autoplay-off"
            title="Autoplay off"
            onClick={() => {
              UIActionCreators.showUIMessage('Autoplay off');
              ImageActionCreators.autoPlayOff();
            }}
          >
            <i className="ion-stop" />
          </button>
        )}

        <button
          className="action-button full-screen"
          title="Fullscreen mode"
          onClick={() => UIActionCreators.toggleFullScreen()}
        >
          <i className="ion-arrow-expand" />
        </button>
      </div>
    </div>
  );
};
