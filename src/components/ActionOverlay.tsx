import React from 'react';

interface Props {
  children: React.ReactNode;
  header?: string;
}

const ActionOverlay = ({ children, header }: Props) => {
  return (
    <div className="ActionOverlay">
      <div className="ActionOverlayBox">
        {header && <div className="ActionOverlay--header">{header}</div>}

        <div className="ActionOverlay--content">{children}</div>
      </div>
    </div>
  );
};

export default ActionOverlay;
