import moment from 'moment';

interface Props {
  date: string;
  message: string;
  user: string;
}

const Comment = ({ date, message, user }: Props) => {
  const dateMoment = moment(date);
  const formattedDate = dateMoment.format('L');
  const formattedTime = dateMoment.format('hh:mm');

  return (
    <div className="comment">
      <div className="comment-header">
        <span className="date">{formattedDate}</span>
        <span className="time">{formattedTime}</span>
        <span className="user">{user}: </span>
      </div>
      <div className="message" dangerouslySetInnerHTML={{ __html: message }}></div>
    </div>
  );
};

export default Comment;
