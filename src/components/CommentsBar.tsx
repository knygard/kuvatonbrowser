import Comment from './Comment';

interface Props {
  comments: { date: string; message: string; user: string }[];
}

const CommentsBar = ({ comments }: Props) => {
  if (!comments || comments.length === 0) {
    return null;
  }

  return (
    <div className="comments-bar">
      {comments.map((comment, index) => (
        <Comment key={index} date={comment.date} user={comment.user} message={comment.message} />
      ))}
    </div>
  );
};

export default CommentsBar;
