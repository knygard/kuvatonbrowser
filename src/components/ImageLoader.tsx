import { useEffect, useState } from 'react';

interface Props {
  src: string;
}

const ImageLoader = ({ src }: Props) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(false);

    const img = new window.Image();
    img.onload = () => setLoaded(true);
    img.src = src;
  }, [src]);

  const style = {
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundImage: `url(${src})`,
  };

  return (
    <div className="ImageLoader-container">
      {loaded && <div className="ImageLoader" style={style} key={src} />}
      {!loaded && (
        <div className="ImageLoader">
          <i className="ion-load-d image-load spinner" />
        </div>
      )}
    </div>
  );
};

export default ImageLoader;
