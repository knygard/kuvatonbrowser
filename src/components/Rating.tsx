interface Props {
  rating: number | string;
}

const Rating = ({ rating: ratingProp }: Props) => {
  const rating = typeof ratingProp === 'string' ? parseInt(ratingProp, 10) : ratingProp;

  if (!rating) {
    return null;
  }

  let ratingStyle;
  if (rating > 0) {
    ratingStyle = 'positive';
  } else if (rating < 0) {
    ratingStyle = 'negative';
  } else {
    ratingStyle = 'neutral';
  }

  return <span className={'Rating ' + ratingStyle}>{rating}</span>;
};

export default Rating;
