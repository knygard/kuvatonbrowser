import classnames from 'classnames';

import ImageLoader from './ImageLoader';

interface Props {
  active?: boolean;
  className?: string;
  onClick: React.MouseEventHandler<HTMLDivElement>;
  src: string;
  style?: React.CSSProperties;
  size?: number;
}

const Thumbnail = ({ active = false, className, src, style, size = 50, ...rest }: Props) => {
  if (!src) {
    return null;
  }

  const classes = classnames('Thumbnail', className, {
    active: active,
  });

  const thumbnailStyle = {
    ...style,
    height: size,
    width: size,
  };

  return (
    <div className={classes} style={thumbnailStyle} {...rest}>
      <ImageLoader src={src} />
    </div>
  );
};

export default Thumbnail;
