import { useState, useEffect } from 'react';
import _ from 'lodash';

import ImageStore from '../stores/ImageStore';
import ImageActions from '../actions/ImageActionCreators';
import Thumbnail from './Thumbnail';

interface Props {
  activeImage: {
    imageId: number;
  };
}

const ThumbnailPane = ({ activeImage }: Props) => {
  const [imagesState, setImages] = useState(ImageStore.getImagesAroundActiveImage(10));

  useEffect(() => {
    const imageListener = () => setImages(ImageStore.getImagesAroundActiveImage(10));

    ImageStore.addChangeListener(imageListener);

    return () => {
      ImageStore.removeChangeListener(imageListener);
    };
  }, []);

  if (!imagesState) {
    return null;
  }

  const thumbSize = 120;

  const activeImageOffset = _.findIndex(imagesState, (i) => i.imageId === activeImage.imageId);

  const thumbnailPaneStyle = {
    height: thumbSize,
  };

  return (
    <div className="ThumbnailPane" style={thumbnailPaneStyle}>
      {imagesState.map((image, index) => {
        const indexOffset = activeImageOffset - index;

        const thumbnailStyle = {
          marginLeft: -thumbSize * indexOffset - thumbSize / 2,
        };

        return (
          <Thumbnail
            src={image.thumbnailUrl}
            onClick={() => {
              ImageActions.setActiveImage(image.imageId);
            }}
            size={thumbSize}
            active={image.imageId === activeImage.imageId}
            key={'thumb' + index}
            style={thumbnailStyle}
          />
        );
      })}
    </div>
  );
};

export default ThumbnailPane;
