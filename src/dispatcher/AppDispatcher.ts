import { Dispatcher } from 'flux';
import assign from 'object-assign';

const AppDispatcher = assign(new Dispatcher(), {
  handleViewAction(action: {
    actionType: string;
    imageId?: number;
    UIMessage?:
      | string
      | {
          visible: string;
          hidden: string;
        };
  }) {
    (this as any).dispatch({
      source: 'VIEW_ACTION',
      action: action,
    });
  },
});

export default AppDispatcher;
