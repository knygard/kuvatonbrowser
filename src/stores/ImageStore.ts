/* eslint-disable prefer-const */

import { EventEmitter } from 'events';
import _ from 'lodash';

import AppDispatcher from '../dispatcher/AppDispatcher';
import KuvatonAPI from '../api/KuvatonAPI';

type Image = {
  imageId: number;
  imageUrl: string;
  comments: {
    date: string;
    message: string;
    user: string;
  }[];
  rating: number;
  title: string;
  thumbnailUrl: string;
  browseUrl: string;
};

const imageBatchSize = 10;

// save status here if there are no previous images to load
let _noMorePrevious = false;

// save status here if there are no more images to load next
let _noMoreNext = false;

let _loadingMoreImages = 0;
let _activeImageId: undefined | number;
let _images: Image[] = [];
let _loadedImageSrcs: string[] = [];

// autoPlay off/on
let _autoPlay = false;

function getImageById(imageId: number) {
  return _images.find((img) => img.imageId === imageId);
}

function getNextImage(image?: Image) {
  let currentIndex = image ? _images.indexOf(image) : -1;

  let nextImage;
  if (!image || currentIndex === -1) {
    nextImage = _.first(_images);
  } else if (_images.length > currentIndex + 1) {
    nextImage = _images[currentIndex + 1];
  } else {
    return null;
  }

  return nextImage;
}

function getPreviousImage(image?: Image) {
  let currentIndex = image ? _images.indexOf(image) : -1;

  let previousImage;
  if (!image || currentIndex === -1) {
    previousImage = _.first(_images);
  } else if (currentIndex === 0) {
    return null;
  } else {
    previousImage = _images[currentIndex - 1];
  }

  return previousImage;
}

function preLoadImage(image: Image, onloadCallback?: () => void) {
  if (!image || _loadedImageSrcs.indexOf(image.imageUrl) !== -1) {
    if (onloadCallback) {
      onloadCallback();
    }
    return;
  }

  _loadedImageSrcs.push(image.imageUrl);

  let img = new window.Image();

  if (onloadCallback) {
    img.onload = onloadCallback;
  }

  img.src = image.imageUrl;
}

function preLoadImagesAroundImage(image: Image) {
  let preLoadThese = [getNextImage(image), getPreviousImage(image)];

  preLoadImage(image, () => {
    preLoadThese.forEach((img, index) => {
      if (img) {
        setTimeout(() => {
          preLoadImage(img);
        }, (index + 1) * 500);
      }
    });
  });
}

async function setActiveImage(image: Image) {
  _activeImageId = image.imageId;
  return preLoadImagesAroundImage(image);
}

function addImages(newImages: Image[]) {
  _images = _([..._images, ...newImages])
    .uniqBy((image) => image.imageId)
    .sortBy((image) => -image.imageId)
    .value();
}

function imagesLeft(direction: 'next' | 'previous') {
  let currentIndex = _.findIndex(_images, { imageId: _activeImageId });

  if (direction === 'next') {
    return _images.length - (currentIndex + 1);
  }

  return currentIndex;
}

const ImageStore = {
  ...EventEmitter.prototype,

  getImages() {
    return _images;
  },

  getActiveImage() {
    return _.find(_images, { imageId: _activeImageId });
  },

  getLastImage() {
    return _.last(_images);
  },

  loadingMoreImages() {
    return _loadingMoreImages === 0;
  },

  getImagesAroundActiveImage(howMany: number) {
    if (!_activeImageId) {
      return [];
    }

    let activeImage = this.getActiveImage();
    let activeImageIndex = _.findIndex(_images, activeImage);

    let firstIndex = activeImageIndex - howMany / 2;
    if (firstIndex < 0) {
      firstIndex = 0;
    }

    let lastIndex = activeImageIndex + howMany / 2;
    if (lastIndex > _images.length - 1) {
      lastIndex = _images.length - 1;
    }

    return _images.slice(firstIndex, lastIndex + 1);
  },

  autoPlayIsOn() {
    return _autoPlay;
  },

  emitChange() {
    (this as any).emit('change');
  },

  addChangeListener(callback: () => void) {
    (this as any).on('change', callback);
  },

  removeChangeListener(callback: () => void) {
    (this as any).removeListener('change', callback);
  },
};

async function loadNewestImages() {
  _loadingMoreImages += 1;

  return KuvatonAPI.images(imageBatchSize)
    .then((response) => {
      addImages(response.data);
      return response;
    })
    .finally(() => {
      _loadingMoreImages -= 1;
    });
}

async function loadMoreImages(imageId?: number, direction?: 'next' | 'previous') {
  if (direction === 'next' && _noMoreNext) {
    return;
  } else if (direction === 'previous' && _noMorePrevious) {
    return;
  } else if (_loadingMoreImages > 1) {
    return;
  }

  _loadingMoreImages += 1;

  return KuvatonAPI.images(imageBatchSize, imageId, direction)
    .then((response) => {
      if (response.data.length < imageBatchSize && direction === 'next') {
        _noMoreNext = true;
      } else if (response.data.length < imageBatchSize && direction === 'previous') {
        _noMorePrevious = true;
      }

      addImages(response.data);

      return response;
    })
    .finally(() => {
      _loadingMoreImages -= 1;
    });
}

AppDispatcher.register(async (payload) => {
  const action = (payload as any).action as {
    imageId?: number;
    actionType:
      | 'INIT'
      | 'NAVIGATE_NEXT_IMAGE'
      | 'NAVIGATE_PREVIOUS_IMAGE'
      | 'SET_ACTIVE_IMAGE'
      | 'AUTOPLAY_ON'
      | 'AUTOPLAY_OFF';
  };

  const activeImage = ImageStore.getActiveImage();

  switch (action.actionType) {
    case 'INIT':
      if (!activeImage) {
        await loadNewestImages();
      }
      ImageStore.emitChange();
      break;

    case 'NAVIGATE_NEXT_IMAGE':
      let next = getNextImage(ImageStore.getActiveImage());
      if (next) {
        await setActiveImage(next);
      }

      if (!_noMoreNext && imagesLeft('next') < 3) {
        await loadMoreImages(ImageStore.getActiveImage()?.imageId, 'next');
      }

      ImageStore.emitChange();

      break;
    case 'NAVIGATE_PREVIOUS_IMAGE':
      let prev = getPreviousImage(ImageStore.getActiveImage());
      if (prev) {
        await setActiveImage(prev);
      }

      if (!_noMorePrevious && imagesLeft('previous') < 3) {
        await loadMoreImages(ImageStore.getActiveImage()?.imageId, 'previous');
      }

      ImageStore.emitChange();
      break;

    case 'SET_ACTIVE_IMAGE':
      if (action.imageId === undefined) {
        await loadNewestImages();
        setActiveImage(_.first(_images)!);
        ImageStore.emitChange();
        break;
      }

      await Promise.all([
        loadMoreImages(action.imageId + 1, 'next'),
        loadMoreImages(action.imageId, 'previous'),
      ]);

      const image = getImageById(action.imageId);

      if (image) {
        setActiveImage(image);
      }

      ImageStore.emitChange();

      break;

    case 'AUTOPLAY_ON':
      _autoPlay = true;
      break;
    case 'AUTOPLAY_OFF':
      _autoPlay = false;
      break;

    default:
      return true;
  }

  ImageStore.emitChange();

  return true;
});

export default ImageStore;
