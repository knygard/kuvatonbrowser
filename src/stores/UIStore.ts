import assign from 'object-assign';
import { EventEmitter } from 'events';

import AppDispatcher from '../dispatcher/AppDispatcher.js';

const visibility = {
  comments: true,
  titlePane: true,
  actionButtons: false,
  info: false,
  thumbnailPane: true,
  help: false,
};

let UIOverlayMessage: string | undefined | null;

const UIStore = assign({}, EventEmitter.prototype, {
  getUIState() {
    return visibility;
  },

  getOverlayMessage() {
    return UIOverlayMessage;
  },

  emitChange() {
    (this as any).emit('change');
  },

  addChangeListener(callback: () => void) {
    (this as any).on('change', callback);
  },

  removeChangeListener(callback: () => void) {
    (this as any).removeListener('change', callback);
  },
});

AppDispatcher.register((payload) => {
  const action = (payload as any).action as {
    actionType: string;
    UIMessage?:
      | string
      | {
          visible: string;
          hidden: string;
        };
  };

  switch (action.actionType) {
    case 'UI_HIDE_COMMENTS':
      if (visibility.comments === true) {
        visibility.comments = false;
        UIStore.emitChange();
      }
      break;
    case 'UI_SHOW_COMMENTS':
      if (visibility.comments === false) {
        visibility.comments = true;
        UIStore.emitChange();
      }
      break;
    case 'UI_TOGGLE_COMMENTS':
      visibility.comments = !visibility.comments;

      const msg = action.UIMessage;

      if (!msg) {
        throw new Error('Missing message');
      }

      if (typeof msg !== 'object') {
        throw new Error('Invalid type of message');
      }

      UIOverlayMessage = visibility.comments ? msg.visible : msg.hidden;

      UIStore.emitChange();
      break;
    case 'UI_TOGGLE_TITLE_PANE':
      visibility.titlePane = !visibility.titlePane;
      UIStore.emitChange();
      break;
    case 'UI_TOGGLE_THUMBNAIL_PANE':
      visibility.thumbnailPane = !visibility.thumbnailPane;
      UIStore.emitChange();
      break;
    case 'UI_SHOW_MESSAGE':
      if (typeof action.UIMessage !== 'string') {
        throw new Error('Invalid message type');
      }

      UIOverlayMessage = action.UIMessage;
      UIStore.emitChange();
      break;
    case 'UI_SHOW_ACTIONBUTTONS':
      if (visibility.actionButtons === false) {
        visibility.actionButtons = true;
        UIStore.emitChange();
      }
      break;
    case 'UI_HIDE_ACTIONBUTTONS':
      if (visibility.actionButtons === true) {
        visibility.actionButtons = false;
        UIStore.emitChange();
      }
      break;
    case 'OVERLAY_MESSAGE_RESET':
      UIOverlayMessage = null;
      UIStore.emitChange();
      break;

    case 'UI_SHOW_HELP':
      if (!visibility.help) {
        visibility.help = true;
        UIStore.emitChange();
      }
      break;
    case 'UI_HIDE_HELP':
      if (visibility.help) {
        visibility.help = false;
        UIStore.emitChange();
      }
      break;
    case 'UI_TOGGLE_HELP':
      visibility.help = !visibility.help;
      UIStore.emitChange();
      break;

    default:
      return true;
  }

  return true;
});

export default UIStore;
